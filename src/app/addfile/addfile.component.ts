import { Component, OnInit } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { WebService } from '../app.service';
import { FileModel } from '../app.models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-addfile',
  templateUrl: './addfile.component.html',
  styleUrls: ['./addfile.component.css']
})
export class AddfileComponent implements OnInit {

  form: FormGroup;
  model = new FileModel();
  error: string;
  private _file: File;

  constructor(private _router: Router,
              private _webService: WebService, 
              private _route: ActivatedRoute,
              private _location: Location,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      name: [this.model.Name, Validators.required],
      access: [this.model.Access, Validators.required],
      description: [this.model.Description, Validators.required]
    });
    this._webService.isAuthenticated().then(data => {
      if (data.Result) {
        this._route.params.subscribe(params => this.model.Folder = +params['parentId']);
      } else {
        this._router.navigate(['./auth']);
      }
    });
  }

  onSubmit() {
    console.log(this.model);
    if (this._file == null) {
      this.error = "File not selected";
      return;
    }
    this._webService.uploadFile(this._file).then(data => {
      if (data.Error == "Ok") {
        this.model.Id = data.Result;
        this.saveFileInfo();
      } else {
        this.error = data.Error;
      }
    });
  }

  fileChange(event) {
    this._file = null;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this._file = fileList[0];
      console.log(this._file);
    }
  }

  saveFileInfo() {
    this._webService.addFile(this.model).then(data => {
      if (data.Error == 'Ok') {
        this._location.back();
      } else {
        this.error = data.Error;
      }
    });
  }

}
