import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { WebService } from '../app.service';
import { FolderModel } from '../app.models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-addfolder',
  templateUrl: './addfolder.component.html',
  styleUrls: ['./addfolder.component.css']
})
export class AddfolderComponent implements OnInit {

  form: FormGroup;
  model = new FolderModel();
  error: string;

  constructor(private _router: Router,
              private _webService: WebService,
              private _route: ActivatedRoute,
              private _location: Location,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      name: [this.model.Name, Validators.required],
      access: [this.model.Access, Validators.required],
      description: [this.model.Description, Validators.required]
    });
    console.log('add folder here');
    this._webService.isAuthenticated().then(data => {
      if (data.Result) {
        this._route.params.subscribe(params => this.model.Parent = +params['parentId']);
      } else {
        this._router.navigate(['./auth']);
      }
    });
  }

  onSubmit() {
    console.log(this.model);
    this._webService.addFolder(this.model).then(data => {
      if (data.Error == 'Ok') {
        this._location.back();
      } else {
        this.error = data.Error;
      }
    });
  }

}
