import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private _router: Router, private _webService: WebService) { }

  ngOnInit() {
    // this._webService.isAuthenticated().then(data => {
    //   let path = './auth';
    //   if (data.Result) {
    //     path = './box';
    //   }
    //   this._router.navigate([path]);
    // })
  }

}
