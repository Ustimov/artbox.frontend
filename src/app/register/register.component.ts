import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterModel } from '../app.models';
import { WebService } from '../app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;
  error = '';
  model = new RegisterModel();

  constructor(private _router: Router,
              private _webService: WebService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      fullName: [this.model.FullName, Validators.required],
      email: [this.model.Email, Validators.required],
      password: [this.model.Password, Validators.required],
      confirmPassword: [this.model.ConfirmPassword, Validators.required]
    });
    this._webService.isAuthenticated().then(data => {
      if (data.Result) {
        this._router.navigate(['./box']);
      }
    });
  }

  onSubmit(): void {
    this.error = '';
    if (this.model.Password != this.model.ConfirmPassword) {
      this.error = "Passwords doesn't match";
      return;
    }
    this._webService.register(this.model).then(data => {
      if (data.Error != 'Ok') {
        this.error = data.Error;
      } else {
        this._router.navigate(['./auth']);
      }
    });
  }
}
