import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { WebService } from '../app.service';
import { FileModel } from '../app.models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {

  form: FormGroup;
  model: FileModel;
  error: string;

  constructor(private _router: Router,
              private _webService: WebService,
              private _route: ActivatedRoute,
              private _location: Location,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this._route.params.subscribe(params => {
      console.log(params['id']);
      this._webService.getFileInfo(params['id']).then(data =>  {
          this.model = data.Result;
            this.form = this._formBuilder.group({
            name: [this.model.Name, Validators.required],
            access: [this.model.Access, Validators.required],
            description: [this.model.Description, Validators.required]
          });
        });
    });
  }

  onSubmit() {
    this._webService.updateFile(this.model).then(data => {
      if (data.Error == 'Ok') {
        this._location.back();
      } else {
        this.error = data.Error;
      }
    });
  }

}
