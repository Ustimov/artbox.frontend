import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FolderModel } from '../app.models';
import { WebService } from '../app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent implements OnInit {

  form: FormGroup;
  model: FolderModel;
  error: string;

  constructor(private _router: Router,
              private _webService: WebService,
              private _route: ActivatedRoute,
              private _location: Location,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this._route.params.subscribe(params => {
      this._webService.getFolderInfo(params['id']).then(data => {
          this.model = data.Result;
          this.form = this._formBuilder.group({
          name: [this.model.Name, Validators.required],
          access: [this.model.Access, Validators.required],
          description: [this.model.Description, Validators.required]
        });
      });
    })
  }

  onSubmit() {
    this._webService.updateFolder(this.model).then(data => {
      if (data.Error == 'Ok') {
        this._location.back();
      } else {
        this.error = data.Error;
      }
    });
  }

}
