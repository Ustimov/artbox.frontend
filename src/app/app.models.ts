export class AuthModel {
  public Email: string;
  public Password: string;
}

export class ResponseModel<T> {
  public Error: string;
  public Result: T;
}

export class RegisterModel {
  public Email: string;
  public FullName: string;
  public Password: string;
  public ConfirmPassword: string;
}

export class FolderModel {
  public Id: number;
  public Name: string;
  public Parent: number;
  public DateCreated: Date;
  public Access: number;
  public Description: string;
  public User: string;
}

export class FileModel {
  public Id: number;
  public Name: string;
  public Folder: number;
  public DateCreated: Date;
  public DateChanged: Date;
  public Type: string;
  public Size: number;
  public Access: number;
  public Description: string;
  public User: string;
  public Path: string;
}

export class BoxModel {
  public Id: number;
  public Folders: FolderModel[];
  public Files: FileModel[];
}

export class FullSearchModel {
  public Query: string;
  public InName: boolean;
  public InDescription: boolean;
  public InType: boolean;
  public DateCreatedFrom: Date;
  public DateCreatedTo: Date;
  public DateChangedFrom: Date;
  public DateChangedTo: Date;
}

export class ChangePasswordModel {
  public OldPassword: string;
  public Password: string;
  public ConfirmPassword: string;
}

export class ProfileModel {
  public FullName: string;
}