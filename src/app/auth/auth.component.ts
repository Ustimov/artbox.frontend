import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthModel } from '../app.models';
import { WebService } from '../app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  form: FormGroup;
  error = '';
  model = new AuthModel();

  constructor(private _router: Router,
              private _webService: WebService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      email: [this.model.Email, Validators.required],
      password: [this.model.Password, Validators.required]
    });
    this._webService.isAuthenticated().then(data => {
      if (data.Result) {
        this._router.navigate(['./box']);
      }
    });
  }

  onSubmit() {
    this.error = '';
    this._webService.auth(this.model).then(data => {
      if (data.Error != "Ok") {
        this.error = data.Error;
      } else {
        localStorage.setItem('email', this.model.Email);
        localStorage.setItem('password', this.model.Password);
        this._router.navigate(['./box']);
        window.location.reload();
      }
    })
  }

}
