import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebService } from '../app.service';
import { BoxModel, FullSearchModel } from '../app.models';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css']
})
export class BoxComponent implements OnInit {

  model: BoxModel;
  fullSearch = false;
  error: string;
  query: string;
  search = new FullSearchModel();

  constructor(private _router: Router,
              private _webService: WebService,
              private _route: ActivatedRoute) { }

  ngOnInit() {
    this._webService.isAuthenticated().then(data => {
      if (data.Result) {
        this._route.params.subscribe(params => {
          this._webService.getBox(params['id']).then(data => this.model = data.Result);
        });
      } else {
        this._router.navigate(['./auth']);
      }
    })
  }

  fullSearchClick(event) {
    console.log('full search click handled')
    event.preventDefault();
    this.fullSearch = !this.fullSearch;
  }


  onFileDelete(event, id) {
    event.preventDefault();
    console.log('File delete ' + id);
    this._webService.deleteFile(id).then(data => {
      if (data.Error == 'Ok') {
        console.log("OnInitNext");
        this.ngOnInit();
      } else {
        console.log(this.error);
        this.error = data.Error;
      }
    });
  }

  onFolderDelete(event, id) {
    event.preventDefault();
    console.log('Folder delete ' + id);
    this._webService.deleteFolder(id).then(data => {
      if (data.Error == 'Ok') {
        this.ngOnInit();
      } else {
        this.error = data.Error;
      }
    });
  }

  onShortSearch(event) {
    event.preventDefault();
    console.log(this.query);
    this._webService.shortSearch(this.query).then(data => {
      if (data.Error == 'Ok') {
        this.model = data.Result;
      } else {
        this.error = data.Error;
      }
    });
  }

  onFullSearch(event) {
    event.preventDefault();
    //this.search.Type = +this.search.Type;
    console.log(this.search);
    this._webService.fullSearch(this.search).then(data => {
      if (data.Error == 'Ok') {
        console.log('Full search ok');
        this.model = data.Result;
      } else {
        console.log('Full search fail');
        this.error == data.Error;
      }
    })
  }

}
