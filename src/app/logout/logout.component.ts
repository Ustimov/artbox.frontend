import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebService } from '../app.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private _router: Router, private _webService: WebService) { }

  ngOnInit() {
    this._webService.logout().then(data => {
      console.log('LogOut');
      this._router.navigate(['./box']);
      window.location.reload();
    });
  }

}
