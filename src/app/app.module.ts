import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { WebService } from './app.service';

import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { RegisterComponent } from './register/register.component';
import { BoxComponent } from './box/box.component';
import { LogoutComponent } from './logout/logout.component';
import { FileComponent } from './file/file.component';
import { FolderComponent } from './folder/folder.component';
import { HeaderComponent } from './header/header.component';
import { AddfolderComponent } from './addfolder/addfolder.component';
import { AddfileComponent } from './addfile/addfile.component';
import { DrawerComponent } from './drawer/drawer.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    RegisterComponent,
    BoxComponent,
    LogoutComponent,
    FileComponent,
    FolderComponent,
    HeaderComponent,
    AddfolderComponent,
    AddfileComponent,
    DrawerComponent,
    ChangepasswordComponent,
    ProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      { path: '', component: BoxComponent },
      { path: 'box', component: BoxComponent },
      { path: 'box/:id', component: BoxComponent },
      { path: 'auth', component: AuthComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'logout', component: LogoutComponent },
      { path: 'file/:id', component: FileComponent },
      { path: 'folder/:id', component: FolderComponent },
      { path: 'addfolder/:parentId', component: AddfolderComponent },
      { path: 'addfile/:parentId', component: AddfileComponent },
      { path: 'changepassword', component: ChangepasswordComponent },
      { path: 'profile', component: ProfileComponent }
    ])
  ],
  providers: [WebService],
  bootstrap: [AppComponent, HeaderComponent, DrawerComponent]
})
export class AppModule { }
