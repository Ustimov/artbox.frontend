import { Component, OnInit } from '@angular/core';
import { WebService } from '../app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  fullName: string;

  constructor(private _webService: WebService) { }

  ngOnInit() {
    this._webService.getProfile().then(data => {
      if (data.Error == "Ok") {
        this.fullName = data.Result.FullName;
      }
    });
  }

}
