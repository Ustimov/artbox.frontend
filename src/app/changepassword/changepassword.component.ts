import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebService } from '../app.service';
import { ChangePasswordModel } from '../app.models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  form: FormGroup;
  model = new ChangePasswordModel();
  error: string;

  constructor(private _router: Router,
              private _webService: WebService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      oldPassword: [this.model.OldPassword, Validators.required],
      password: [this.model.Password, Validators.required],
      confirmPassword: [this.model.ConfirmPassword, Validators.required]
    });
    this._webService.isAuthenticated().then(data => {
      if (!data.Result) {
        this._router.navigate(['./auth']);
      }
    });
  }

  onSubmit() {
    console.log(this.model);
    if (this.model.Password != this.model.ConfirmPassword) {
      this.error = "Passwords doesn't match";
    } else {
      this._webService.changePassword(this.model).then(data => {
        if (data.Error != "Ok") {
          this.error = data.Error;
        } else {
          this._router.navigate(['./box']);
        }
      })
    }
  }

}
