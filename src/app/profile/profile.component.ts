import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebService } from '../app.service';
import { ProfileModel } from '../app.models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  form: FormGroup;
  model = new ProfileModel();
  error: string;
  private _file: File;

  constructor(private _router: Router,
              private _webService: WebService,
              private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form = this._formBuilder.group({
      fullName: [this.model.FullName, Validators.required]
    });
    this._webService.isAuthenticated().then(data => {
      if (!data.Result) {
        this._router.navigate(['./auth']);
      } else {
        this._webService.getProfile().then(data => {
          if (data.Error == "Ok") {
            console.log(data.Result);
            this.model = data.Result;
          } else {
            this.error = data.Error;
          }
        });
      }
    });
  }

  onSubmit() {
    console.log(this.model);
    console.log(this._file);
    if (this._file != null) {
      this._webService.uploadPhoto(this._file).then(data => {
        if (data.Error != "Ok") {
          this.error = data.Error;
        }
      });
    }
    this._webService.updateProfile(this.model).then(data => {
      if (data.Error != "Ok") {
        this.error = data.Error;
      } else {
        this._router.navigate(['./box']);
      }
    });
  }

  fileChange(event) {
    this._file = null;
    let fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this._file = fileList[0];
    }
  }

}
