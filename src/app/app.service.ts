import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { AuthModel, BoxModel, FileModel, FolderModel, ProfileModel, RegisterModel, ResponseModel } from './app.models';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class WebService {
  
  //private _baseUrl = 'http://127.0.0.1:8083';
  
  private _baseUrl = 'http://localhost:15225';

  private _options = new RequestOptions(
      { headers: new Headers({'Content-Type': 'application/json'}), 
      withCredentials: true });

  constructor(private _http: Http) { 
      console.log("new webservice");
  }

  auth(credentials: AuthModel): Promise<ResponseModel<any>> {
    console.log('auth');
    return this._http
      .post(this._baseUrl + '/auth', credentials, this._options)
      .toPromise()
      .then(response => {
          let r = response.json() as ResponseModel<any>;
          console.log(r);
          return r;
        });
  }

  register(registerInfo: RegisterModel): Promise<ResponseModel<any>> {
    console.log('register')
    return this._http
      .post(this._baseUrl + '/register', registerInfo)
      .toPromise()
      .then(response => {
           let r = response.json() as ResponseModel<any>;
           console.log(r);
           return r;
        });
  }

  isAuthenticated(): Promise<ResponseModel<boolean>> {
    console.log('isAuth');
    return this._http.get(this._baseUrl, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<boolean>);
  }

  logout(): Promise<ResponseModel<any>> {
      return this._http.post(this._baseUrl + '/logout', '', this._options)
        .toPromise()
        .then(response => response.json() as ResponseModel<any>);
  }

  getBox(id): Promise<ResponseModel<BoxModel>> {
      console.log('box');
      let path = '/box';
      if (id) {
        path += '/' + id;
      }
      console.log('Path is' + path);
      return this._http.get(this._baseUrl + path, this._options)
        .toPromise()
        .then(resonse => {
            let r = resonse.json() as ResponseModel<BoxModel>;
            console.log(r);
            return r;
        });
  }

  getFileInfo(id: string): Promise<ResponseModel<FileModel>> {
    return this._http.get(this._baseUrl + '/file/' + id, this._options)
      .toPromise()
      .then(response => {
        let r = response.json() as ResponseModel<FileModel>;
        console.log(r);
        return r;
      });
  }

  getFolderInfo(id: string): Promise<ResponseModel<FolderModel>> {
    return this._http.get(this._baseUrl + '/folder/' + id, this._options)
      .toPromise()
      .then(response => {
        let r = response.json() as ResponseModel<FolderModel>;
        console.log(r);
        return r;
      });
  }

  updateFile(file: FileModel): Promise<ResponseModel<any>> {
    return this._http.put(this._baseUrl + '/file', file, this._options)
      .toPromise()
      .then(response => {
        let r = response.json() as ResponseModel<any>;
        console.log(r);
        return r;
      });
  }

  updateFolder(folder: FolderModel): Promise<ResponseModel<any>> {
    return this._http.put(this._baseUrl + '/folder', folder, this._options)
      .toPromise()
      .then(response => {
        let r = response.json() as ResponseModel<any>;
        console.log(r);
        return r;
      });
  }

  uploadFile(file): Promise<ResponseModel<number>> {
    let formData = new FormData();
    console.log(file);
    formData.append('file', file, file.name);
    let headers = new Headers();
    //headers.append('Content-Type', undefined);
    let options = new RequestOptions(
      { headers: headers, withCredentials: true });
    return this._http.post(this._baseUrl + '/upload', formData, options)
      .toPromise()
      .then(response => response.json() as ResponseModel<number>);
  }

  addFile(file: FileModel): Promise<ResponseModel<any>> {
    return this._http.post(this._baseUrl + '/file', file, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<any>);
  }

  addFolder(folder: FolderModel): Promise<ResponseModel<any>> {
    return this._http.post(this._baseUrl + '/folder', folder, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<any>);
  }

  deleteFile(id): Promise<ResponseModel<any>> {
    return this._http.delete(this._baseUrl + '/file/' + id, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<any>);
  }

  deleteFolder(id): Promise<ResponseModel<any>> {
    return this._http.delete(this._baseUrl + '/folder/' + id, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<any>);
  }

  shortSearch(query): Promise<ResponseModel<BoxModel>> {
    return this._http.post(this._baseUrl + '/search', { Query: query }, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<BoxModel>);
  }

  fullSearch(search): Promise<ResponseModel<BoxModel>> {
    return this._http.post(this._baseUrl + '/fullsearch', search, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<BoxModel>);
  }

  changePassword(changePasswordModel): Promise<ResponseModel<any>> {
    return this._http.post(this._baseUrl + '/changepassword', changePasswordModel, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<any>);
  }

  getProfile(): Promise<ResponseModel<ProfileModel>> {
    return this._http.get(this._baseUrl + '/profile', this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<ProfileModel>);
  }

  updateProfile(profile): Promise<ResponseModel<any>> {
    return this._http.post(this._baseUrl + '/profile', profile, this._options)
      .toPromise()
      .then(response => response.json() as ResponseModel<any>);
  }

  uploadPhoto(file): Promise<ResponseModel<any>> {
    let formData = new FormData();
    console.log(file);
    formData.append('file', file, file.name);
    let headers = new Headers();
    let options = new RequestOptions(
      { headers: headers, withCredentials: true });
    return this._http.post(this._baseUrl + '/uploadphoto', formData, options)
      .toPromise()
      .then(response => response.json() as ResponseModel<any>);
  }

}