import { ArtBox.FrontendPage } from './app.po';

describe('art-box.frontend App', () => {
  let page: ArtBox.FrontendPage;

  beforeEach(() => {
    page = new ArtBox.FrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
